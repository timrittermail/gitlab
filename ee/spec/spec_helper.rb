# frozen_string_literal: true

require Rails.root.join("spec/support/helpers/stub_requests.rb")

Dir[Rails.root.join("ee/spec/support/helpers/*.rb")].each { |f| require f }
Dir[Rails.root.join("ee/spec/support/shared_contexts/*.rb")].each { |f| require f }
Dir[Rails.root.join("ee/spec/support/shared_examples/*.rb")].each { |f| require f }
Dir[Rails.root.join("ee/spec/support/**/*.rb")].each { |f| require f }

RSpec.configure do |config|
  config.include EE::LicenseHelpers

  include StubSaasFeatures

  config.define_derived_metadata(file_path: %r{ee/spec/}) do |metadata|
    # For now, we assign a starter license for ee/spec
    metadata[:with_license] = metadata.fetch(:with_license, true)

    location = metadata[:location]
    metadata[:geo] = metadata.fetch(:geo, true) if %r{[/_]geo[/_]}.match?(location)
  end

  config.define_derived_metadata do |metadata|
    # There's already a value, so do not set a default
    next if metadata.has_key?(:without_license)
    # There's already an opposing value, so do not set a default
    next if metadata.has_key?(:with_license)

    metadata[:without_license] = true
  end

  config.before do |example|
    if example.metadata.fetch(:stub_feature_flags, true)
      # Specs should not require cut_off_date to be expired by default for cloud connector features.
      stub_feature_flags(cloud_connector_cut_off_date_expired: false)
    end
  end

  config.before(:context, :with_license) do
    License.destroy_all # rubocop: disable Cop/DestroyAll
    TestLicense.init
  end

  config.after(:context, :with_license) do
    License.destroy_all # rubocop: disable Cop/DestroyAll
  end

  config.before(:context, :without_license) do
    License.destroy_all # rubocop: disable Cop/DestroyAll
  end

  config.after(:context, :without_license) do
    TestLicense.init
  end

  config.around(:example, :with_cloud_connector) do |example|
    cloud_connector_access = create(:cloud_connector_access)

    example.run
  ensure
    cloud_connector_access.destroy!
  end

  config.around(:each, :geo_tracking_db) do |example|
    example.run if Gitlab::Geo.geo_database_configured?
  end

  config.define_derived_metadata do |metadata|
    metadata[:do_not_stub_snowplow_by_default] = true if metadata.has_key?(:snowplow_micro)
  end

  config.before(:example, :snowplow_micro) do
    config.include(Matchers::Snowplow)

    next unless Gitlab::Tracking.micro_verification_enabled?

    Matchers::Snowplow.clean_snowplow_queue

    stub_application_setting(snowplow_enabled: true)
    stub_application_setting(snowplow_app_id: 'gitlab-test')
  end

  config.include SecretsManagement::GitlabSecretsManagerHelpers, :gitlab_secrets_manager

  config.before(:example, :gitlab_secrets_manager) do
    SecretsManagement::OpenbaoTestSetup.start_server_and_proxy
  end

  config.after(:example, :gitlab_secrets_manager) do
    # For now we'll just clean up kv secrets engines because that's
    # all we're handling in the secrets manager for now. We can add more
    # things to clean up here later on as we add more features.
    clean_all_kv_secrets_engines
  end
end
